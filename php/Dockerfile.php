FROM php:7.2-apache

ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_DOCUMENT_ROOT /var/www/html/public

# Setup server
RUN apt-get update && apt-get install -yq \
	net-tools \
	libicu-dev \
	g++ \
	libmcrypt-dev \
	zlib1g-dev \
	git \
	zip \
	unzip \
	gnupg \
	python3-pip \
	supervisor \
	firefox-esr \
	imagemagick

RUN pecl install xdebug-2.6.1 mcrypt-1.0.2 &&\
	sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf &&\
	sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

# Setup PHP extensions
RUN docker-php-ext-enable xdebug mcrypt
RUN docker-php-ext-configure intl
RUN docker-php-ext-install pdo_mysql zip intl bcmath

# Setup PHP
RUN echo 'memory_limit = 256M' >> /usr/local/etc/php/php.ini
RUN echo 'post_max_size = 1000M' >> /usr/local/etc/php/php.ini
RUN echo 'upload_max_filesize = 1000M' >> /usr/local/etc/php/php.ini
RUN echo 'log_errors = On' >> /usr/local/etc/php/php.ini
RUN echo 'error_log = /dev/stderr' >> /usr/local/etc/php/php.ini

# Config Xdebug for PHP
RUN mkdir /var/log/xdebug && chmod 777 /var/log/xdebug
RUN echo 'xdebug.profiler_enable = 0' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.profiler_enable_trigger = 1' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.profiler_output_dir = /var/log/xdebug' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.profiler_output_name = "%u_%R.out"' >> /usr/local/etc/php/php.ini

# Post-install
RUN curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN composer global require \
	jakub-onderka/php-parallel-lint \
	jakub-onderka/php-console-highlighter \
	pdepend/pdepend \
	phploc/phploc \
	phpmd/phpmd \
	phpunit/phpunit \
	sebastian/phpcpd

RUN a2enmod rewrite
